# PowerShell Scripts : Dispatcher Automation

[![License](https://img.shields.io/badge/License-Apache%202.0-brightgreen.svg)](https://opensource.org/licenses/Apache-2.0)
[![Website](https://img.shields.io/website-up-down-green-red/http/skydevops.co.in.svg?label=skydevops)]()


## Description

Automating Dispatcher when configuration changes
-	Job will track the changes of dispatcher.any, web.config, and rewritemaps.config files.
-	Files are categoried into folders based on environment. 
-	Creates a backup of the file and updates the changes in dispatcher scripts, docroot folders respectively.

## Requirements

- Powershell 	>=	5.x
- Git 			>= 	1.8.x

## License

Licensed under the Apache License V2.0. See the [LICENSE file](LICENSE) for details.

## Author Information

You can find me on Twitter: [@skydevops](https://twitter.com/skydevops)

## Contributors

- Shashi Yebbare ([@skydevops](https://twitter.com/skydevops))